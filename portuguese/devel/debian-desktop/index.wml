#use wml::debian::template title="Debian no Desktop"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896" maintainer="Leonardo Rocha"

<h2>O Sistema Operacional Universal como seu Desktop</h2>

<p>
  O subprojeto Debian Desktop consiste em um grupo de voluntários(as) os(as)
  quais têm como objetivo criar o melhor sistema operacional possível
  para uso doméstico e estação de trabalho corporativa. Nosso lema é <q>software que funciona</q>.
  Resumindo, nosso objetivo é levar o Debian, GNU e o Linux ao grande
  público.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>Nossos princípios</h3>
<ul>
  <li>
    Reconhecendo que ambos
    <a href="https://wiki.debian.org/DesktopEnvironment">Ambientes de Desktop</a>
    existem, iremos suportar seu uso e nos certificar de que funcionem bem
    no Debian.
  </li>
  <li>
    Reconhecemos que existem somente duas classes importantes de
    usuários(as): o(a) novato(a) e o(a) experiente. Faremos o possível para
    tornar as coisas muito fáceis para o(a) novato(a) e ao mesmo tempo
    permitiremos que o(a) usuário(a) experiente configure as coisas da
    maneira que deseja.
  </li>
  <li>
    Nós tentaremos garantir que o software seja configurado para
    o uso desktop mais comum. Por exemplo, a conta de usuário(a) comum
    adicionada por padrão durante a instalação deverá ter permissões
    para utilização de recursos de áudio e vídeo, impressão e
    gerenciamento do sistema através do sudo.
  </li>
  <li>
    <p>
    Tentaremos nos assegurar de que questões feitas ao(à) usuário(a)
    (as quais devem ser mantidas em um nível mínimo) façam sentido mesmo
    para quem possui o mínimo de conhecimento de computadores. Alguns
    pacotes Debian atualmente apresentam ao(à) usuário(a) complicados detalhes
    técnicos. Questões técnicas sobre o debconf apresentadas ao(à) usuário(a)
    pelo debian-installer podem ser evitadas.
    Para o(a) novato(a), isso é frequentemente confuso e assustador.
    Para o(a) experiente, eles podem ser irritantes e desnecessários. Um(a)
    novato(a) pode nem saber sobre o que são essas perguntas. Um(a) experiente
    pode configurar seu ambiente de área de trabalho da forma que desejar
    depois que a instalação tenha sido concluída. A prioridade desses
    tipos de questões sobre o debconf podem ser minimizadas.
    </p>
  <li>
    E nós nos divertiremos fazendo tudo isso!
  </li>
</ul>
<h3>Como você pode ajudar</h3>
<p>
  As partes mais importantes de um subprojeto Debian não são as
  listas de discussão, páginas web ou espaço nos repositórios
  para pacotes. A parte mais importante é <em>gente motivada</em>
  que faz as coisas acontecerem. Você não precisa ser um(a) desenvolvedor(a)
  oficial para começar a fazer pacotes e patches. A equipe principal do
  Debian Desktop se certificará que seu trabalho seja integrado.
  Sendo assim, aqui estão algumas coisas que você pode fazer:
</p>
<ul>
  <li>
    Teste nossa configuração <q>Ambiente Desktop Padrão</q> (ou configuração kde-desktop),
    instalando uma de nossas <a href="$(DEVEL)/debian-installer/">imagens de
    teste da próxima versão (testing)</a> e nos dê um retorno na
    <a href="https://lists.debian.org/debian-desktop/">lista de discussão
    debian-desktop</a> (em inglês).
  </li>
  <li>
    Trabalhe no <a href="$(DEVEL)/debian-installer/">instalador (debian-installer)</a>.
    A interface GTK+ precisa de você.
  </li>
  <li>
    Ajude
    <a href="https://wiki.debian.org/Teams/DebianGnome">a equipe Debian GNOME</a>,
    <a href="https://qt-kde-team.pages.debian.net/">a equipe Debian Qt e KDE</a> ou
    <a href="https://salsa.debian.org/xfce-team/">o grupo Debian Xfce</a>.
    Você pode ajudar com empacotamento, pesquisa de bugs, documentação, teste,
    entre outros.
  </li>
  <li>
    Ensine os(as) usuários(as) como instalar e usar as configurações do desktop Debian que
    nós temos (desktop, gnome-desktop e kde-desktop).
  </li>
  <li>
    Trabalhe para diminuir ou remover perguntas do
    <a href="https://packages.debian.org/debconf">debconf</a> desnecessárias
    de pacotes e fazer com que aquelas necessárias sejam fáceis de serem entendidas.
  </li>
  <li>
    Ajude nos esforços do
    <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop
    Artwork</a> (trabalho artístico do Desktop Debian).
  </li>
</ul>
<h3>Wiki</h3>
<p>
  Nós temos alguns artigos em nossa wiki, e nosso ponto de partida está aqui:
  <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Alguns dos
  artigos do Desktop Debian estão desatualizados.
</p>
<h3>Mailing List</h3>
<p>
  Este subprojeto está sendo debatido na lista de discussão
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a> (em
  inglês).
</p>
<h3>Canal IRC</h3>
<p>
  Encorajamos qualquer pessoa (desenvolvedor(a) Debian ou não) que esteja
  interessada no Debian Desktop a se juntar a nós no canal #debian-desktop
  na rede de IRC <a href="http://oftc.net/">OFTC</a> (irc.debian.org).
</p>
<h3>Quem está envolvido(a)?</h3>
<p>
  Qualquer um(a) que queira se juntar é bem-vindo (a). Na verdade, todos nos grupos pkg-gnome
  pkg-kde e pkg-xfce estão indiretamente envolvidos(as). Os(As) inscritos(as) na
  lista de discussão debian-desktop são contribuidores(as) ativos(as). Os grupos
  debian-installer e tasksel também são importantes para os nossos objetivos.
</p>

<p>
  Está página web é mantida por <a href="https://people.debian.org/~stratus/">\
  Gustavo Franco</a>. Os mantenedores anteriores eram
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> e
  <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
