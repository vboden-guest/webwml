#use wml::debian::template title="Espelhos do Debian" BARETITLE="true"
#use wml::debian::translation-check translation="6ab524a7b127f07748ca4f2b1168f6ed8b785a81"

<p>O Debian é distribuído por todo o mundo usando os espelhos (<q>mirrors</q>)
para fornecer aos(as) usuários(as) um melhor acesso aos nossos repositórios.</p>

<p>Os seguintes repositórios Debian são espelhados:</p>

<dl>
<dt><strong>Pacotes do Debian</strong> (<code>debian/</code>)</dt>
  <dd>O conjunto de pacotes Debian -- isso inclui uma grande quantidade de
      pacotes .deb, os materiais de instalação e os fontes.
      <br>
      Veja a lista de <a href="list">espelhos do Debian que incluem
      o repositório <code>debian/</code></a>.
  </dd>
<dt><strong>Imagens de CD</strong> (<code>debian-cd/</code>)</dt>
  <dd>O repositório de imagens de CD: arquivos jigdo e arquivos de imagens ISO.
      <br>
      Veja a lista de <a href="$(HOME)/CD/http-ftp/">espelhos do Debian que
      incluem o repositório <code>debian-cd/</code></a>.
  </dd>
<dt><strong>Versões antigas</strong> (<code>debian-archive/</code>)</dt>
  <dd>O arquivo de versões antigas do Debian.
      <br>
      Consulte <a href="$(HOME)/distrib/archive">arquivos de distribuição</a>
      para obter mais informações.
  </dd>
</dl>

<h2>Informações para proprietários(as) de espelhos</h2>

<p>Os espelhos do Debian são mantidos por voluntários(as); portanto, se não houver bons espelhos
perto de você e você estiver em condições de doar espaço em disco e conectividade,
considere criar um espelho e contribuir para tornar o Debian mais acessível.
Veja as páginas sobre como <a href="ftpmirror">configurar um espelho do aquivo Debian</a>
para mais informações sobre espelhamento..</p>
