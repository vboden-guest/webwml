#use wml::debian::template title="Informazioni su come usare il motore di ricerca Debian"
#use wml::debian::translation-check translation="c3b565ac6b9deb00572865c557a9c7094047163a" maintainer="Johan Haggi"

<p>Il motore di ricerca Debian a <a href="https://search.debian.org/">https://search.debian.org/</a>
permette diversi tipi di ricerche, dipendenti da cosa vorreste
trovare.</p>

<h3>Ricerca semplice (Simple Search)</h3>

<p>Il metodo più semplice consiste nell'immettere una singola parola nel box search e
poi premere invio (o cliccare sul bottone <em>Search</em>). Il motore di ricerca
darà tutte le pagine del sito web che contengono quella parola. Ciò darà
abbastanza spesso dei buoni risultati.</p>

<p>Il livello immediatamente superiore consiste nel ricercare più di una
parola che darà le pagine che contengono tutte le parole immesse.</p>


<h3>Ricerca booleana (Boolean Search)</h3>

<p>Se una ricerca semplice non fosse sufficiente, potrebbe servire una ricerca
<a href="https://foldoc.org/boolean">booleana</a>.
Si potrà scegliere tra <em>AND</em>, <em>OR</em>,
<em>NOT</em> oppure una qualsiasi combinazione di questi tre. Nota: questi
operatori devono essere scritti in maiuscolo.</p>

<p><b>AND</b> darà come risultato le pagine contenenti entrambe le
parole. Per esempio "gcc AND patch" mostrerà tutte le pagine che contengono
sia "gcc" sia "patch". Questo esempio darà gli stesi risultati della ricerca
di "gcc patch" tuttavia l'uso esplicito di AND permette di fare combinazioni
con gli altri operatori.</p>

<p><b>OR</b> darà come risultato le pagine contenenti una delle
parole. Per esempio "gcc OR patch" mostrerà qualsiasi pagine che contenga
o "gcc" o "patch".</p>

<p><b>NOT</b> esclude una parola dal risultato.
Per esempio "gcc NOT patch" darà come risultato tutte le pagine che
contengono "gcc" e non contengono "patch". È possibile scrivere "gcc AND
NOT patch" per ottenere il medesimo risultato, la ricerca di "NOT patch"
non è supportata.</p>

<p><b>(</b>...<b>)</b> per unire gruppi di sottoespressioni. Per esempio
"(gcc OR make) NOT patch" troverà tutte le pagine che contengono o "gcc" o
"make" e non contengono "patch".</p>
