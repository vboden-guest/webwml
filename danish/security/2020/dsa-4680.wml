#use wml::debian::translation-check translation="0cfee114b2d0c22d16e262ece0a0804aac60d237" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i Tomcats servlet og JSP-motor, hvilke kunne 
medføre smugling af HTTP-forespørgsler, udførelse af kode i AJP-connector'en 
(som standard deaktiveret i Debian) eller manden i midten-angreb mod 
JMX-grænsefladen.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i version 
9.0.31-1~deb10u1.  Rettelsen af 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">\
CVE-2020-1938</a> kan kræve opsætningsændringer, når Tomcat anvendes med 
AJP-connector'en, fx i kombination med libapache-mod-jk.  For eksempel er 
attributten <q>secretRequired</q> nu som standard opsat til true.  I 
påvirkede opsætninger, anbefales man at gennemgå 
<a href="https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html">\
https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html</a>, før opdateringen 
bliver udrullet.</p>

<p>Vi anbefaler at du opgraderer dine tomcat9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende tomcat9, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4680.data"
