#use wml::debian::translation-check translation="7e2e121907227c63046c343586a659966635cee3" maintainer="Szabolcs Siebenhofer"
<define-tag description>LTS biztonsági frissítés</define-tag>
<define-tag moreinfo>
<p>Néhány biztonsági problémát találtak az ImageMagik-ban, a kép 
manipulációs programcsomagban. A támadó szolgáltatás megtagadást 
tud okozni és tetszőleges kódot tud futtatni, ha speciálisan átlakított 
képet dolgoznak fel.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14528">CVE-2017-14528</a>

    <p>A TIFFSetProfiles funkcó a coders/tiff.c fájlban inkorrekt 
    elvárásokkal rendelkezik a LibTIFF TIFFGetField visszatérési értékével 
    kapcsolatban, ami lehetővé szolgáltatás megtagadás okozását teszi a 
    távoli támadó számára módosított file esetén (A TIFFSetField hibás hivása után
    a use-after-free összedönti az alkalmazást.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19667">CVE-2020-19667</a>

    <p>Verem puffer túlcsordulás és feltétel nélküli ugrás a ReadXPMImage-ben, a 
    coders/xpm.c fájlban.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25665">CVE-2020-25665</a>

    <p>A PALM kép kódoló a coders/palm.c fájlban helytelen hívást kezdeményez a 
    AcquireQuantumMemory() felé a WritePALMImage() rutinban, mert 256-al kell
    eltolni. Ez később határon kívüliolvasást okoz a rutinban. Ennek hatása van a 
    megbízhatóságra.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25674">CVE-2020-25674</a>

    <p>WriteOnePNGImage() a coders/png.c fájlból (a PNG kódoló) helytelen 
    kilépési kondíciójú for ciklussal rendelkezik, ami határon kívüli 
    READ-et tesz lehetővé heap-buffer-overflow-n keresztül. Ez azért lehetséges 
    mert a színtérkép 256-nál kevesebb érvényes értéket enged meg, de a cikluss
    256 alkalommal fut le, ezzel megpróbál érvénytelen színtérkép adatokat próbál 
    átadni az eseménynalpónak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27560">CVE-2020-27560</a>

    <p>Az ImageMagik nullával való osztást tesz lehetővé a MagickCore/layer.c 
    fájlban a OptimizeLayerFrames funkcóban, ami szolgáltatás megtagadást 
    okozhat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27750">CVE-2020-27750</a>

    <p>Hiba volt a MagickCore/colorspace-private.h és a 
    MagickCore/quantum.h fájlban. A távoli támadó, aki egy speciálisan kialakított 
    fájlt tölt fel, meghatározhatatlan viselkedést válthat ki `unsigned char`
    típus tartományán kívüli matematikai nulla osztással. Ez nagy valószínűséggel 
    hatással lesz az alkalmazások elérhetőségére, de potenciálisan más problémákat 
    is okozhat a meghatározatlan viselkedés miatt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27760">CVE-2020-27760</a>

    <p>A /MagickCore/enhance.c fájl `GammaImage ()` fájljában a `gamma` 
    értéktől függően lehetséges a nullával osztás feltétel aktiválása, 
    amikor a speciálisam kialakított bemeneti fájlt az ImageMagick 
    feldolgozza.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27763">CVE-2020-27763</a>

    <p>Hibát találtak a MagickCore / resize.c fájlban. Az a támadó, aki egy 
    speciálisan elkészített fájlt küld be, amelyet az ImageMagick dolgoz fel, 
    meghatározhatatlan viselkedést válthat ki matematikai nullával való osztás 
    formájában. Ez nagy valószínűséggel hatással lesz az alkalmazások 
    elérhetőségére, de más problémákat is okozhat a meghatározatlan viselkedés 
    miatt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27765">CVE-2020-27765</a>

    <p>A MagickCore / segment.c fájlban hibát találtak. Az a támadó, aki 
    egy speciálisan elkészített fájlt küld be, amelyet az ImageMagick 
    dolgoz fel, meghatározhatatlan viselkedést válthat ki matematikai 
    nullával való osztás formájában. Ez nagy valószínűséggel hatással 
    lesz az alkalmazások elérhetőségére, de potenciálisan más 
    problémákat is okozhat a meghatározatlan viselkedése miatt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27773">CVE-2020-27773</a>

    <p>Hiba volt a MagickCore/gem-private.h fájlban. A távoli támadó, aki 
    egy speciálisan kialakított fájlt tölt fel, meghatározhatatlan 
    viselkedést válthat ki `unsigned char` típus tartományán kívüli 
    matematikai nulla osztással. Ez nagy valószínűséggel 
    hatással lesz az alkalmazások elérhetőségére, de potenciálisan más problémákat 
    is okozhat a meghatározatlan viselkedés miatt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29599">CVE-2020-29599</a>

    <p>Az ImageMagick rosszul kezeli az -authenticate opciót, amely 
    lehetővé teszi a jelszó beállítását a jelszóval védett PDF 
    fájlokhoz. A felhasználó által vezérelt jelszót nem sikerült 
    elkerülni/megtisztítani, ezért további shell parancsokat lehetett 
    bebevinni a coders / pdf.c keresztül.</p></li>

</ul>

<p>A Debian 9 <q>stretch</q> esetén a probléma a 8:6.9.7.4+dfsg-11+deb9u11 verzióban 
javításra került.</p>

<p>Azt tanácsoljuk, hogy frissítsd az imagemagick csomagjaidat.</p>

<p>Az imagemagick csomag biztonság állapotával kapcsolatban lásd a bizonsági 
nyomkövető oldalát:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>További információk a Debian LTS biztonági figyelmeztetéseiről, hogyan tudod ezeket a 
frissítéseket a rendszereden telepíteni és más gyakran feltett kérdések megtalálhatóak itt: 
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2523.data"
# $Id: $
