#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Irssi possède quelques problèmes où des attaquants distants pouvaient
provoquer un plantage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9468">CVE-2017-9468</a>

<p>Dans irssi, lors de réception d’un message DCC sans hôte ou pseudo d'origine,
un essai de déréférencement de pointeur NULL est réalisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9469">CVE-2017-9469</a>

<p>Dans irssi, lors de la réception de fichiers DCC avec des guillemets
incorrects, un essai pour trouver le guillemet final un octet avant la fin de la
mémoire allouée est réalisé.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.8.15-5+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets irssi.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1088.data"
# $Id: $
