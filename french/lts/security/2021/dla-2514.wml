#use wml::debian::translation-check translation="fd63d2a146d25251b70add97ad931027fb37733e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans flac, une bibliothèque pour le
codec de compression audio sans perte <q>Free Lossless Audio Codec</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6888">CVE-2017-6888</a>

<p>Fuite de mémoire à l'aide d'un fichier FLAC contrefait spécialement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0499">CVE-2020-0499</a>

<p>Lecture hors limites due à un dépassement de tampon de tas.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.3.2-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets flac.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de flac, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/flac">https://security-tracker.debian.org/tracker/flac</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2514.data"
# $Id: $
