#use wml::debian::translation-check translation="0e5fba72646f0245319e61c704c66bf7dffcb46f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il existait une vulnérabilité de dépassement d’entier concernant la longueur
de trames websocket reçues à l'aide d'une connexion websocket. Un attaquant
pourrait utiliser ce défaut pour provoquer une attaque par déni de service sur
un serveur HTTP autorisant des connexions websocket.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.1.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-websocket.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-websocket, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-websocket">https://security-tracker.debian.org/tracker/golang-websocket</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2520.data"
# $Id: $
