#use wml::debian::translation-check translation="0706dcba0990f075e843c8aa509e0772d5ed71b9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de client ont été découvertes dans OpenSSH, l’outil
vedette de connectivité pour une connexion distante d’interpréteur sécurisé et
transfert de fichiers sécurisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

<p>Dans scp.c, le client scp permettait aux serveurs SSH distants de contourner
les restrictions d’accès voulues à l’aide du nom de fichier « . » ou vide.
L’impact modifiait les permissions du répertoire cible du côté client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

<p>Dû à un encodage de caractères manquant dans l’affichage de progression, un
serveur malveillant (ou un attaquant de type « homme du milieu ») pouvait employer
des noms d’objet contrefaits pour manipuler la sortie du client, par exemple,
en utilisant des codes de contrôle ANSI pour cacher le transfert de fichiers
supplémentaires. Cela affecte refresh_progress_meter() dans progressmeter.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

<p>Du fait que l’implémentation de scp est dérivée de rcp 1983, le serveur
choisit quels fichiers ou répertoires sont envoyés au client. Cependant, le
client scp ne réalise qu'une validation superficielle du nom d’objet renvoyé
(seules les attaques par traversée de répertoires sont empêchées). Un serveur
scp malveillant (ou un attaquant de type « homme du milieu ») pouvait écraser des
fichiers arbitraires dans le répertoire cible du client scp. Si une opération
récursive (-r) était réalisée, le serveur pouvait manipuler des sous-répertoires,
ainsi que, par exemple, écraser le fichier .ssh/authorized_keys.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:6.7p1-5+deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1728.data"
# $Id: $
