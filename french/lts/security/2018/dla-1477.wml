#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15501">CVE-2018-15501</a>

<p>Une lecture potentielle hors limites lors du traitement du paquet smart
<q>ng</q> pourrait conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10887">CVE-2018-10887</a>

<p>Un défaut a été découvert qui pourrait conduire à un dépassement d'entier qui
à son tour conduirait à une lecture hors limite, permettant de lire avant
l’objet de base. Cela pourrait être utilisé pour divulguer des adresses de
mémoire ou causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10888">CVE-2018-10888</a>

<p>Un défaut pourrait conduire à une lecture hors limite lors de la lecture d’un
fichier binaire delta. Cela pourrait aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.21.1-3+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libgit2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1477.data"
# $Id: $
