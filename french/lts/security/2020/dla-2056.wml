#use wml::debian::translation-check translation="8e8d2afbd226569371372239ff102b8c50b12f4e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de dissimulation de requête HTTP dans waitress,
un serveur WSGI entièrement en Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16789">CVE-2019-16789</a>

<p>Dans waitress jusqu’à la version 1.4.0, si un serveur mandataire est utilisé
devant waitress, une requête non valable peut être envoyée par un attaquant,
contournant le frontal, et qui est analysée différemment par waitress,
conduisant à une dissimulation potentielle de requête HTTP. Des requêtes
spécialement contrefaites contenant des caractères d’espace particuliers dans
l’en-tête Transfer-Encoding seront analysées par Waitress comme étant une
requête « chunked », mais un serveur frontal utilisera l’en-tête
Content-Length au lieu de Transfer-Encoding considéré comme non valable car
contenant des caractères non valables. Si un serveur frontal réalise un
pipeline HTTP vers un serveur dorsal waitress, cela pourrait conduire à un
découpage de requête HTTP aboutissant à un empoisonnement potentiel du cache
ou à une divulgation d'informations inattendues. Ce problème est corrigé dans
waitress 1.4.1 à l’aide d’une validation plus stricte des champs HTTP.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.8.9-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets waitress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2056.data"
# $Id: $
