#use wml::debian::translation-check translation="092a02ead2ab4eb8292cda8032bc290df5633cb1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans les modules du démon IRC
d'InspIRCd, qui pourraient provoquer un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20917">CVE-2019-20917</a>

<p>Le module mysql avant la version 3.3.0 contient un déréférencement de
pointeur NULL lorsqu'il est construit avec mariadb-connector-c. Lorsqu'elle
est combinée avec les modules sqlauth ou sqloper, cette vulnérabilité peut
être utilisée par n’importe quel utilisateur pouvant se connecter à un serveur
InspIRCd pour faire planter à distance le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25269">CVE-2020-25269</a>

<p>Le module pgsql contient une vulnérabilité d’utilisation de mémoire après
libération. Lorsqu'elle est combinée avec les modules sqlauth ou sqloper, cette
vulnérabilité peut être utilisée par n’importe quel utilisateur pouvant se
connecter à un serveur InspIRCd pour faire planter à distance le serveur.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.23-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets inspircd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de inspircd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/inspircd">https://security-tracker.debian.org/tracker/inspircd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2375.data"
# $Id: $
