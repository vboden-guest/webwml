#use wml::debian::translation-check translation="394d51b2741a5ff2c1f8f7e11a456dc7e8129ef7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une élévation des privilèges a été découverte dans Sympa, un gestionnaire
moderne de liste de diffusion. Elle est corrigée lorsque Sympa est utilisé
conjointement avec des MTA courants (tels Exim ou Postfix) en désactivant
un exécutable setuid. Cependant, aucun correctif n’est actuellement disponible
pour tous les environnements (tel sendmail). De plus, une vulnérabilité de
redirection ouverte a été découverte et corrigée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26880">CVE-2020-26880</a>

  <p>Sympa permet une élévation locale des privilèges, de compte utilisateur
  de sympa à un accès complet de superutilisateur, en modifiant le fichier
  de configuration sympa.conf (appartenant à sympa) et en l’analysant à l’aide
  de l’exécutable setuid sympa_newaliases-wrapper.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000671">CVE-2018-1000671</a>

  <p>Sympa contient un CWE-601 : vulnérabilité de redirection d’URL vers un
  site non fiable (redirection ouverte) dans le paramètre <q>referer</q>
  de l’action de connexion wwsympa.fcgi. Cela peut aboutir à une
  redirection ouverte et à un script intersite réfléchi à l’aide d’URI
  de données.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 6.2.16~dfsg-3+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sympa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sympa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sympa">https://security-tracker.debian.org/tracker/sympa</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2441.data"
# $Id: $
