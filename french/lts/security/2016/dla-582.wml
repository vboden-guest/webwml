#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libidn. Le projet
« Common Vulnerabilities and Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8948">CVE-2015-8948</a>

<p>Il survient une lecture hors limite quand idn lit un octet vide en
entrée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6261">CVE-2016-6261</a>

<p>Une lecture de pile hors limites est exploitable dans idna_to_ascii_4i.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6263">CVE-2016-6263</a>

<p>Le rejet par stringprep_utf8_nfkc_normalize de chaînes UTF-8 non
valables, provoque un plantage.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.25-2+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libidn.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-582.data"
# $Id: $
