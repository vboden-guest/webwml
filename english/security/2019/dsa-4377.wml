<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The ESnet security team discovered a vulnerability in rssh, a restricted
shell that allows users to perform only scp, sftp, cvs, svnserve
(Subversion), rdist and/or rsync operations. Missing validation in the
scp support could result in the bypass of this restriction, allowing the
execution of arbitrary shell commands.</p>

<p>Please note that with the update applied, the <q>-3</q> option of scp can no
longer be used.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 2.3.4-5+deb9u1.</p>

<p>We recommend that you upgrade your rssh packages.</p>

<p>For the detailed security status of rssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rssh">\
https://security-tracker.debian.org/tracker/rssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4377.data"
# $Id: $
