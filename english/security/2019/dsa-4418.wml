<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in the Dovecot email server. When reading
FTS or POP3-UIDL headers from the Dovecot index, the input buffer size
is not bounds-checked. An attacker with the ability to modify dovecot
indexes, can take advantage of this flaw for privilege escalation or the
execution of arbitrary code with the permissions of the dovecot user.
Only installations using the FTS or pop3 migration plugins are affected.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1:2.2.27-3+deb9u4.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4418.data"
# $Id: $
