<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9862">CVE-2020-9862</a>

    <p>Ophir Lojkine discovered that copying a URL from the Web Inspector
    may lead to command injection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9893">CVE-2020-9893</a>

    <p>0011 discovered that a remote attacker may be able to cause
    unexpected application termination or arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9894">CVE-2020-9894</a>

    <p>0011 discovered that a remote attacker may be able to cause
    unexpected application termination or arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9895">CVE-2020-9895</a>

    <p>Wen Xu discovered that a remote attacker may be able to cause
    unexpected application termination or arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9915">CVE-2020-9915</a>

    <p>Ayoub Ait Elmokhtar discovered that processing maliciously crafted
    web content may prevent Content Security Policy from being
    enforced.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9925">CVE-2020-9925</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to universal cross site scripting.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.28.4-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4739.data"
# $Id: $
