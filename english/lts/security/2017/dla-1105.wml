<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>clamav is vulnerable to multiple issues that can lead
to denial of service when processing untrusted content.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6418">CVE-2017-6418</a>

    <p>out-of-bounds read in libclamav/message.c, allowing remote attackers
    to cause a denial of service via a crafted e-mail message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6420">CVE-2017-6420</a>

    <p>use-after-free in the wwunpack function (libclamav/wwunpack.c), allowing
    remote attackers to cause a denial of service via a crafted PE file with
    WWPack compression.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.99.2+dfsg-0+deb7u3.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1105.data"
# $Id: $
