<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Fredric discovered a couple of buffer overflows in MilkyTracker, of which,
a brief description is given below.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14464">CVE-2019-14464</a>

    <p>XMFile::read in XMFile.cpp in milkyplay in MilkyTracker had a heap-based
    buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14496">CVE-2019-14496</a>

    <p>LoaderXM::load in LoaderXM.cpp in milkyplay in MilkyTracker had a
    stack-based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14497">CVE-2019-14497</a>

    <p>ModuleEditor::convertInstrument in tracker/ModuleEditor.cpp in MilkyTracker
    had a heap-based buffer overflow.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.90.85+dfsg-2.2+deb8u1.</p>

<p>We recommend that you upgrade your milkytracker packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1961.data"
# $Id: $
