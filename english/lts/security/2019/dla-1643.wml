<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>krb5, a MIT Kerberos implementation, had several flaws in LDAP DN
checking, which could be used to circumvent a DN containership check by
supplying special parameters to some calls.
Further an attacker could crash the KDC by making S4U2Self requests.</p>


<p>For Debian 8 <q>Jessie</q>,
these problems have been fixed in version 1.12.1+dfsg-19+deb8u5.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1643.data"
# $Id: $
