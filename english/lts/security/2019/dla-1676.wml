<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Ralph Dolmans and Karst Koymans found a flaw in the way unbound, a
validating, recursive, caching DNS resolver, validated
wildcard-synthesized NSEC records.</p>

<p>An improperly validated wildcard NSEC record could be used to prove the
non-existence (NXDOMAIN answer) of an existing wildcard record, or trick
unbound into accepting a NODATA proof.</p>

<p>For more information please refer to the upstream advisory at
<url "https://unbound.net/downloads/CVE-2017-15105.txt">.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4.22-3+deb8u4.</p>

<p>We recommend that you upgrade your unbound packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1676.data"
# $Id: $
