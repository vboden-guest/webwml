<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was potential denial of service
vulnerability in libxml2, the Gnome XML parsing library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19956">CVE-2019-19956</a>

    <p>xmlParseBalancedChunkMemoryRecover in parser.c in libxml2 before 2.9.10 has a memory leak related to newDoc-&gt;oldNs.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.9.1+dfsg1-5+deb8u8.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2048.data"
# $Id: $
