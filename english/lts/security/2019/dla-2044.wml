<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>There has been an out-of-bounds write in Cyrus SASL leading to
unauthenticated remote denial-of-service in OpenLDAP via a malformed LDAP
packet. The OpenLDAP crash was ultimately caused by an off-by-one error
in _sasl_add_string in common.c in cyrus-sasl.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.26.dfsg1-13+deb8u2.</p>

<p>We recommend that you upgrade your cyrus-sasl2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2044.data"
# $Id: $
