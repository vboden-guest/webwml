<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The File Manager (gollem) module in Horde Groupware has allowed remote
attackers to bypass Horde authentication for file downloads via a crafted
fn parameter that corresponded to the exact filename.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.0.10-1+deb9u2.</p>

<p>We recommend that you upgrade your php-horde-gollem packages.</p>

<p>For the detailed security status of php-horde-gollem please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-horde-gollem">https://security-tracker.debian.org/tracker/php-horde-gollem</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2352.data"
# $Id: $
