<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a vulnerability in the lynis security
auditing tool. The license key could be obtained by simple observation of the
process list when a data upload is being performed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13033">CVE-2019-13033</a>

    <p>In CISOfy Lynis 2.x through 2.7.5, the license key can be obtained by
    looking at the process list when a data upload is being performed. This
    license can be used to upload data to a central Lynis server. Although no
    data can be extracted by knowing the license key, it may be possible to
    upload the data of additional scans.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.3-1+deb8u1.</p>

<p>We recommend that you upgrade your lynis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2253.data"
# $Id: $
