#use wml::debian::cdimage title="Live 安装映像"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="fc74a202c0f1463d4bc08c88f3da3c2b4f1066f1"
#include "$(ENGLISHDIR)/releases/images.data"

<p><q>Live 安装映像</q>包含一个 Debian 系统，可以在不修改\
硬盘驱动器上的任何文件的情况下进行启动，并可根据\
映像内容安装 Debian。
</p>

<p><a name="choose_live"><strong>Live 映像适合我吗？</strong></a>这里有一些需要考虑的事，\
可能会帮助你作出决定。
<ul>
<li><b>风格：</b>Live 映像有几种<q>风格</q>，不同风格\
各自提供了一种桌面环境（GNOME、KDE、LXDE、Xfce、\
Cinnamon 或 MATE）。大多数用户\
会觉得这些初始软件包的选择是合适的，以后再通过网络\
安装他们需要的附加软件包。
<li><b>架构：</b>目前仅提供了两种最受欢迎架构的映像，\
32 位 PC（i386）和 64 位 PC（amd64）。
<li><b>安装程序：</b>从 Debian 10 Buster 开始，Live 映像包含了\
对最终用户友好的 <a href="https://calamares.io">Calamares 安装程序</a>，\
代替著名的 \<a href="$(HOME)/devel/debian-installer">Debian 安装程序</a>\
。这是一个和发行版无关的安装程序框架。
<li><b>大小：</b>每个映像都远小于全套的 \
DVD 映像，但大于网络安装[CN:介质:][HKTW:媒介:]。
<li><b>语言：</b>映像没有包含完整的语言\
[CN:支持:][HKTW:支援:]软件包。如果您需要用于您语言的输入法、字体和补充语言\
软件包，则您需要以后再安装它们。
</ul>

<p>以下 Live 安装映像可供下载：</p>
 
<ul>

  <li><q>稳定（stable）</q>版本的官方<q>Live 安装</q>映像 &mdash; <a
  href="#live-install-stable">见下</a></li>

</ul>


<h2 id="live-install-stable"><q>稳定（stable）</q>版本的官方 Live 安装映像</h2>

<p>如上所述，这些映像具有不同的风格和大小，\
适合先尝试由一组精选的[CN:默认:][HKTW:缺省:]软件包构成的
Debian 系统，然后再通过相同的[CN:介质:][HKTW:媒介:]安装。</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB（通过 <a href="$(HOME)/CD/torrent-cd">BitTorrent</a> 下载）</strong></p>
<p><q>混合</q> ISO 映像文件，适合写入 DVD-R(W) [CN:介质:][HKTW:媒介:]，\
或者大小合适的 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]。如果您可以使用 BitTorrent，请尽量使用，以减小我们\
服务器的负载。</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
<p><q>混合</q> ISO 映像文件，适合写入 DVD-R(W) [CN:介质:][HKTW:媒介:]，\
或者大小合适的 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]。</p>
	  <stable-live-install-iso-cd-images />
</div>
</div>

<p>有关这些[CN:文件:][HKTW:档案:]是什么，以及\
如何使用这些[CN:文件:][HKTW:档案:]，请参阅 \
<a href="../faq/">FAQ</a>。</p>

<p>如果您打算使用下载的 Live 映像安装 Debian，\
请确保查看\
<a href="$(HOME)/releases/stable/installmanual">有关\
安装过程的详细信息</a>。</p>

<p>有关 Debian Live 项目提供的 Debian Live 系统的更多信息，\
请参见 <a href="$(HOME)/devel/debian-live">Debian Live 项目网页</a>。</p>

