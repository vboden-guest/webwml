#use wml::debian::template title="Debian verkrijgen"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="fc74a202c0f1463d4bc08c88f3da3c2b4f1066f1"

<p>Debian wordt <a href="../intro/free">op een vrije manier</a>
verspreid via het Internet. U kunt het geheel downloaden van een van onze <a
href="ftplist">spiegelservers</a>.
De <a href="../releases/stable/installmanual">installatiehandleiding</a> bevat
gedetailleerde installatie-instructies.
En de aantekeningen bij de release zijn <a href="../releases/stable/releasenotes">hier</a> te vinden.
</p>

<p>Als u simpelweg Debian wilt installeren beschikt u over de volgende
opties:</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Een installatie-image downloaden</a></h2>
    <p>
      Afhankelijk van uw internetverbinding, kunt u één van de volgende
      bestanden downloaden:
    </p>
   <ul>
     <li>Een <a href="netinst"><strong>klein installatie-image</strong></a>:
          kan snel gedownload worden en moet op een verwijderbare schijf geschreven
          worden. Om deze te kunnen gebruiken, heeft uw machine een internetverbinding
          nodig.
      <ul class="quicklist downlist">
        <li><a title="Het installatieprogramma voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bits
            pc netinst iso</a></li>
        <li><a title="Het installatieprogramma voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bits
            pc netinst iso</a></li>
      </ul>
     </li>
     <li>Een groter <a href="../CD/"><strong>compleet
      installatie-image</strong></a>: bevat meer pakketten, zodat het eenvoudiger te
      installeren is op een machine zonder internetverbinding.
      <ul class="quicklist downlist">
        <li><a title="Dvd-torrents voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/bt-dvd/">64-bits pc torrents (dvd)</a></li>
        <li><a title="Dvd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/bt-dvd/">32-bits pc torrents (dvd)</a></li>
        <li><a title="Cd-torrents voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/bt-cd/">64-bits pc torrents (cd)</a></li>
        <li><a title="Cd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/bt-cd/">32-bits pc torrents (cd)</a></li>
      </ul>
     </li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Een Debian cloudimage gebruiken</a></h2>
    <ul>
      <li>Een officieel <a href="https://cloud.debian.org/images/cloud/"><strong>cloudimage</strong></a>:
            gebouwd door het Debian Cloud Team en rechtstreeks te gebruiken bij uw cloudprovider.
        <ul class="quicklist downlist">
          <li><a title="OpenStackimage voor 64-bits Intel en AMD Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">64-bits AMD/Intel OpenStack (Qcow2)</a></li>
          <li><a title="OpenStackimage voor 64-bits ARM Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">64-bits ARM OpenStack (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
   <h2><a href="../CD/live/">Debian vóór het installeren uitproberen</a></h2>
   <p>
      U kunt Debian ook eerst uitproberen door een live-systeem te starten
      vanaf een cd, dvd of USB-stick, zonder bestanden op de computer te
      installeren. Wanneer u klaar bent, kunt u het bijgevoegde
      installatieprogramma (vanaf Debian 10 Buster is dit het
      gebruiksvriendelijke  <a href="https://calamares.io">Calamares
      installatieprogramma</a>) uitvoeren. In de veronderstelling dat deze
      images beantwoorden aan uw vereisten inzake grootte, taal en
      pakketselectie, is dit mogelijk een methode die geschikt is voor u.
      Lees meer uitgebreide <a
      href="../CD/live#choose_live">informatie over deze methode</a> om u nog
      beter in staat te stellen om te beslissen.
   </p>
   <ul class="quicklist downlist">
     <li><a title="Live-torrents voor 64-bits Intel en AMD pc's downloaden"
           href="<live-images-url/>/amd64/bt-hybrid/">64-bits pc live-torrents</a></li>
     <li><a title="Live-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
           href="<live-images-url/>/i386/bt-hybrid/">32-bits pc live-torrents</a></li>
   </ul>
  </div>
</div>
  <div class="line">
    <div class="item col50">
      <h2><a href="../CD/vendors/">Een set cd's of dvd's kopen van een
      leverancier die Debian cd's verkoopt</a></h2>

  <p>
  Veel aanbieders verkopen de distributie voor minder dan 5&nbsp;euro
  (plus porto).
  <br />
  Bij sommige van de <a href="../doc/books">boeken</a> over Debian krijgt
  u ook een gratis cd.
  </p>

  <p>Hieronder staan de belangrijkste voordelen van de cd's:</p>

  <ul>
    <li>Installatie vanaf cd's is meer recht-toe-recht-aan.</li>
    <li>Het is mogelijk om te installeren op machines zonder
    internetverbinding.</li>
    <li>U kunt Debian installeren zonder zelf alle pakketten te hoeven
    downloaden (en op zoveel machines als u wilt).</li>
    <li>De cd's kunnen worden gebruikt om gemakkelijk een beschadigd
    Debian-systeem te repareren.</li>
  </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Een computer kopen met Debian geïnstalleerd</a></h2>
    <p>Dit heeft een aantal voordelen:</p>

    <ul>
    <li>U hoeft Debian zelf niet te installeren.</li>
    <li>De installatie is vooraf geconfigureerd voor de juiste hardware.</li>
    <li>De leverancier levert wellicht ook technische ondersteuning.</li>
    </ul>
  </div>
</div>
