#use wml::debian::template title="Derivaten van Debian"
#use wml::debian::translation-check translation="1b62dcf348569f7b3d0975c674a776e714dc31fc"

<p>
Een <a href="#list">aantal distributies</a> zijn op Debian gebaseerd.
Misschien willen sommige mensen deze distributies eens bekijken <em>ter
aanvulling op</em> de officiële releases van Debian.
</p>

<p>
Een derivaat van Debian is een distributie
die gebaseerd is op het in Debian geleverde werk, maar
een eigen identiteit, eigen doelstellingen en een eigen publiek heeft
en die gecreëerd wordt door een entiteit die onafhankelijk is van Debian.
Derivaten passen Debian aan om de door henzelf gestelde doelen te bereiken.
</p>

<p>
Debian verwelkomt organisaties die een nieuwe, op Debian gebaseerde distributie
willen ontwikkelen en moedigt hen daarin aan.
In de geest van het <a href="$(HOME)/social_contract">sociaal contract</a> van
Debian, hopen we dat derivaten met hun werk zullen bijdragen aan Debian en aan
de bovenstroomse projecten, zodat iedereen van hun verbeteringen kan genieten.
</p>

<h2 id="list">Welke derivaten zijn er?</h2>

<p>
We willen graag de volgende derivaten van Debian belichten:
</p>

## Please keep this list sorted alphabetically
## Please only add derivatives that meet the criteria below
<ul>
    <li>
      <a href="https://grml.org/">Grml</a>:
      live-systeem voor systeembeheerders.
      <a href="https://wiki.debian.org/Derivatives/Census/Grml">Meer info</a>.
    </li>
    <li>
      <a href="https://www.kali.org/">Kali Linux</a>:
      veiligheidsaudits en penetratietests.
      <a href="https://wiki.debian.org/Derivatives/Census/Kali">Meer info</a>.
    </li>
    <li>
      <a href="https://pureos.net/">PureOS van Purism</a>:
	  <a href="https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1">Een door FSF goedgekeurde</a>
	  voortdurend bijgewerkte (rolling) release, die focust op privacy,
      veiligheid en comfort.
      <a href="https://wiki.debian.org/Derivatives/Census/Purism">Meer info</a>.
    </li>
    <li>
      <a href="https://tails.boum.org/">Tails</a>:
      privacy en anonimiteit beschermen.
      <a href="https://wiki.debian.org/Derivatives/Census/Tails">Meer info</a>.
    </li>
    <li>
      <a href="https://www.ubuntu.com/">Ubuntu</a>:
      Linux over de hele wereld populair maken.
      <a href="https://wiki.debian.org/Derivatives/Census/Ubuntu">Meer info</a>.
    </li>
</ul>

<p>
Daarnaast worden op Debian gebaseerde distributies vermeld in de
<a href="https://wiki.debian.org/Derivatives/Census">telling van Debian derivaten</a>
en op <a href="https://wiki.debian.org/Derivatives#Lists">andere plaatsen</a>.
</p>

<h2>Waarom een derivaat gebruiken in plaats van Debian?</h2>

<p>
Indien u een specifieke behoefte heeft, waaraan beter beantwoord wordt door een derivaat, gebruikt u die misschien liever in plaats van Debian.
</p>

<p>
Indien u behoort tot een specifieke gemeenschap of groep mensen en er voor
die groep een derivaat bestaat, gebruikt u die misschien liever in plaats van
Debian.
</p>

<h2>Waarom is Debian in derivaten geïnteresseerd?</h2>

<p>
Derivaten brengen Debian tot bij een groter aantal mensen met meer
uiteenlopende ervaringen en andere eisen dan het publiek dat wijzelf momenteel
bereiken.
Door relaties aan te gaan met derivaten, informatie over hen in de
infrastructuur van Debian
<a href="https://wiki.debian.org/Derivatives/Integration">op te nemen</a>
en door aanpassingen die zij maakten, terug in Debian te integreren,
delen wij onze ervaringen met onze derivaten,
vergroten we ons inzicht in onze derivaten en hun doelpubliek,
verruimen we misschien de Debian gemeenschap en maken we
Debian geschikt voor een diverser publiek.
</p>

<h2>Welke derivaten stelt Debian in het licht?</h2>

## Examples of these criteria are in the accompanying README.txt
<p>
De hierboven belichte derivaten beantwoorden alle aan de meeste van de
volgende criteria:
</p>

<ul>
    <li>werken actief samen met Debian</li>
    <li>worden actief onderhouden</li>
    <li>er is een team mensen bij betrokken, waaronder minstens één lid van
        Debian</li>
    <li>namen deel aan de telling van derivaten van Debian en voegden een
        sources.list toe aan hun derivatentellingspagina</li>
    <li>hebben een bijzonder kenmerk of aandachtsgebied</li>
    <li>zijn opmerkelijke en gevestigde distributies</li>
</ul>

<h2>Waarom een Debian derivaat maken?</h2>

<p>
Het gaat sneller om een bestaande distributie aan te passen dan helemaal van
nul te beginnen, aangezien verpakkingsformaat, basispakketten en andere zaken
gedefinieerd en bruikbaar zijn.
Een heleboel software is reeds verpakt, waardoor het niet nodig is om tijd te
besteden aan het verpakken van de meeste dingen.
Dit stelt derivaten in staat te focussen op de behoeften van een specifiek
doelpubliek.
</p>

<p>
Debian garandeert dat wat we distribueren door derivaten <a href="$(HOME)/intro/free">vrij</a>
mag aangepast worden en verder verspreid naar hun doelpubliek.
We doen dit door de licenties te controleren van de software die we verspreiden
en deze te toetsen aan de <a href="$(HOME)/social_contract#guidelines">Debian Free Software Guidelines (DFSG)</a>, de richtlijnen van Debian inzake vrije software.
</p>

<p>
In Debian vinden derivaten een aantal verschillende <a href="$(HOME)/releases/">release</a>-cycli, waarop zij hun distributie kunnen baseren.
Dit geeft derivaten de mogelijkheid om
<a href="https://wiki.debian.org/DebianExperimental">experimentele</a> software
uit te proberen, om echt <a href="$(HOME)/releases/unstable/">snel</a> te gaan
in hun ontwikkeling, <a href="$(HOME)/releases/testing/">vaak</a> te updaten
met een kwaliteitsgarantie, een <a href="$(HOME)/releases/stable/">solide basis</a>
voor hun werk te hebben, <a href="https://backports.debian.org/">recentere</a>
software bovenop een solide basis te gebruiken, te genieten van
<a href="$(HOME)/security/">veiligheidsondersteuning</a> en deze ondersteuning
<a href="https://wiki.debian.org/LTS">te verlengen</a>.
</p>

<p>
Debian ondersteunt een aantal verschillende <a href="$(HOME)/ports/">architecturen</a>
en medewerkers <a href="https://wiki.debian.org/DebianBootstrap">werken</a>
aan methodes om er automatisch nieuwe te creëren voor nieuwe processortypes.
Dit stelt derivaten in staat de hardware van hun keuze te gebruiken of nieuwe
processorontwerpen te ondersteunen.
</p>

<p>
De Debian-gemeenschap en mensen uit bestaande derivaten staan ter beschikking
en zijn bereid om nieuwe distributies bij hun werk te helpen met richtlijnen.
</p>

<p>
Derivaten worden voor een aantal redenen gemaakt, zoals
vertaling naar nieuwe talen,
ondersteuning voor specifieke hardware,
andere installatiemechanismes of
het ondersteunen van een specifieke gemeenschap of groep mensen.
</p>

<h2>Hoe maakt men een derivaat van Debian?</h2>

<p>
Derivaten kunnen zo nodig delen van de infrastructuur (zoals pakketbronnen) van
translation to new languagesDebian gebruiken.
Derivaten moeten verwijzingen naar Debian (zoals het logo, de naam, enz.)
en naar diensten van Debian (zoals de website en het BTS) aanpassen.
</p>

<p>
Indien het doel erin bestaat een set te installeren pakketten te definiëren,
dan zou het creëren van een <a href="$(HOME)/blends/">Debian blend</a>,
een doelgroepspecifieke uitgave binnen Debian een interessante piste kunnen zijn.
</p>

<p>
Gedetailleerde ontwikkelingsinformatie is te vinden in de
<a href="https://wiki.debian.org/Derivatives/Guidelines">richtlijnen</a> en
begeleiding kan men bekomen bij de
<a href="https://wiki.debian.org/DerivativesFrontDesk">onthaaldienst</a>.
</p>
